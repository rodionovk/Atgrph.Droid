package com.atgrph.data.models

/**
 * ATGRPH. Created by KONSTANTIN on 30.11.2017.
 */
class User {
    enum class UserAuthType constructor(val value: Int){
        UNKNOW(0),
        VK(1),
        FB(2),
        EMAIL(3)
    }
}