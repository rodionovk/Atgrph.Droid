package com.atgrph.data.models

/**
 * ATGRPH. Created by KONSTANTIN on 30.11.2017.
 */
data class AuthToken(
        val userId: String,
        val token: String,
        val refreshToken: String,
        val createDate: String,
        val expireDate: String
)