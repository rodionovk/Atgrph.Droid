package com.atgrph.data


import com.atgrph.data.models.AuthToken
import com.atgrph.data.network.ApiFactory
import io.reactivex.Observable
import retrofit2.Response

/**
 * ATGRPH. Created by KONSTANTIN on 30.11.2017.
 */
class AtgrphRepository : IAtgrphRepository {

    override fun getUser(userServerId: String?, token: String): Observable<String> {
        return ApiFactory.getAtgrphService(token)
                .getUser(userServerId!!)
    }

    override fun authWithSocialToken(token: String, deviceId: String, type: Int) : Observable<AuthToken>  {
        return ApiFactory.getAtgrphService()
                .authWithSocialToken(token, deviceId, type)
    }


}