package com.atgrph.data

import com.atgrph.data.models.AuthToken
import io.reactivex.Observable
import retrofit2.Response

/**
 * ATGRPH. Created by KONSTANTIN on 30.11.2017.
 */
interface IAtgrphRepository {

    fun authWithSocialToken(token: String,deviceId: String,type: Int) : Observable<AuthToken>

    fun getUser(userServerId: String?, token: String) : Observable<String>
}