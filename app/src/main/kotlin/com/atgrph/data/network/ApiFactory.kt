package com.atgrph.data.network

import com.atgrph.BuildConfig
import com.atgrph.data.models.AuthToken
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * ATGRPH. Created by KONSTANTIN on 30.11.2017.
 */
class ApiFactory {

    companion object {

        private var client: OkHttpClient? = null
        private var service: AtgrphService? = null

        private val lock = Any()

        fun getAtgrphService(token: String = ""): AtgrphService {
            /*if(service == null)*/ {
                synchronized(lock) {
                    service = createService(token)
                }
            }
            return createService(token) //service!!
        }

        private fun createService(token: String): AtgrphService {
            return Retrofit.Builder()
                    .baseUrl(BuildConfig.API_ENDPOINT)
                    .client(getClientRetrofit(token))
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
                    .create(AtgrphService::class.java)
        }

        private fun getClientRetrofit(token: String): OkHttpClient? {
            /*if(client == null)*/ {
                synchronized(lock) {
                    client = buildClient(token)
                }
            }
            return buildClient(token) //client
        }

        private fun buildClient(token: String): OkHttpClient {
            return OkHttpClient()
                    .newBuilder()
                    .addInterceptor(LoggingInterceptor.create())
                    .addInterceptor(TokenInterceptor.create(token))
                    .build()
        }
    }

}