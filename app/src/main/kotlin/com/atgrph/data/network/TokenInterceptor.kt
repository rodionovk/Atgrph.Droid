package com.atgrph.data.network

import android.text.TextUtils
import okhttp3.Interceptor
import okhttp3.Response

/**
 * ATGRPH. Created by KONSTANTIN on 01.12.2017.
 */
class TokenInterceptor private constructor(val token: String): Interceptor {

    companion object {
        fun create(token: String) : Interceptor {
            return TokenInterceptor(token)
        }
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        if(TextUtils.isEmpty(token)) {
            return chain.proceed(chain.request())
        }

        val request = chain.request().newBuilder()
                .addHeader("X-Auth", token)
                .addHeader("Accept-Language", "ru")
                .addHeader("X-API-Version", "1.4")
                .build()

        return chain.proceed(request)
    }
}