package com.atgrph.data.network

import com.atgrph.data.models.AuthToken
import io.reactivex.Observable
import retrofit2.http.*

/**
 * ATGRPH. Created by KONSTANTIN on 30.11.2017.
 */
interface AtgrphService {

    @FormUrlEncoded
    @POST("token/")
    fun authWithSocialToken(@Field("token") token: String,
                            @Field("deviceId") deviceId: String,
                            @Field("type") type: Int) : Observable<AuthToken>

    /**
     *
     */
    @GET("api/v1.3/users/{userServerId}/")
    fun getUser(@Path("userServerId") userServerId: String) : Observable<String>


}