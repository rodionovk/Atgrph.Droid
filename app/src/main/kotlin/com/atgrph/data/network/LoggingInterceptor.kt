package com.atgrph.data.network

import com.atgrph.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor

/**
 * ATGRPH. Created by KONSTANTIN on 30.11.2017.
 */
class LoggingInterceptor : Interceptor {

    companion object {

        private lateinit var loggingInterceptor: Interceptor

        fun create() : Interceptor {
            loggingInterceptor = HttpLoggingInterceptor().setLevel(
                    when(BuildConfig.DEBUG) {
                        true -> HttpLoggingInterceptor.Level.BODY
                        else -> HttpLoggingInterceptor.Level.NONE
                    }
            )

            return loggingInterceptor
        }
    }

    override fun intercept(chain: Interceptor.Chain?): Response {
        return loggingInterceptor.intercept(chain)
    }
}