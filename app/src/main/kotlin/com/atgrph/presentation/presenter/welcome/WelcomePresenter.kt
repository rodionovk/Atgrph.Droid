package com.atgrph.presentation.presenter.welcome

import com.atgrph.presentation.view.main.Screens
import ru.terrakok.cicerone.Router

/**
 * ATGRPH. Created by KONSTANTIN on 28.11.2017.
 */
class WelcomePresenter(val router: Router) {

    fun init() {
        router.replaceScreen(Screens.WELCOME_AUTOGRAPHS)
    }

    fun nextScreen(key: String) {
        router.replaceScreen(key)
    }
}