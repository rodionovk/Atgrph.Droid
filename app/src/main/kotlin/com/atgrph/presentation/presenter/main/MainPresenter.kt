package com.atgrph.presentation.presenter.main

import com.atgrph.R
import com.atgrph.presentation.view.main.IMainView
import com.atgrph.presentation.view.main.Screens
import ru.terrakok.cicerone.Router

/**
 * ATGRPH. Created by KONSTANTIN on 23.11.2017.
 */
class MainPresenter(val router: Router, val view: IMainView) {

    fun init() {
        router.replaceScreen(Screens.HOME)
    }

    fun tabClick(viewId: Int?) {
        when(viewId){
            R.id.imgHome -> router.replaceScreen(Screens.HOME)
            R.id.imgSearch -> router.replaceScreen(Screens.SEARCH)
            R.id.imgPen -> router.replaceScreen(Screens.PEN)
            R.id.imgNotification -> router.replaceScreen(Screens.NOTIFICATION)
            R.id.imgProfile -> router.replaceScreen(Screens.PROFILE)
        }
    }

    fun backPressed() {
        router.exit();
    }
}