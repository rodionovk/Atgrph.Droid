package com.atgrph.presentation.presenter.feed

import com.atgrph.presentation.view.feed.IFeedView
import com.atgrph.presentation.view.main.Screens
import ru.terrakok.cicerone.Router

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
class FeedPresenter(val router: Router, val view: IFeedView) {

    fun gotoProfile() {
        router.navigateTo(Screens.PROFILE)
    }
}