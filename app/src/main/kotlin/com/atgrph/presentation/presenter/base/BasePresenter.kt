package com.atgrph.presentation.presenter.base

import com.atgrph.presentation.presenter.feed.FeedPresenter
import ru.terrakok.cicerone.Router

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
class BasePresenter(val router: Router) {


    fun exit() {
        router.exit()
    }
}