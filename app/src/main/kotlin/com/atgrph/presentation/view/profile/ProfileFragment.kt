package com.atgrph.presentation.view.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atgrph.R
import com.atgrph.presentation.view.base.BaseFragment

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
class ProfileFragment : BaseFragment() {

    companion object {

        private val EXTRA_TEXT = "text"

        fun newInstance(info: String): ProfileFragment {
            val fragment = ProfileFragment()
            val args = Bundle()
            args.putString(EXTRA_TEXT, info)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_profile, container,false);
        return view
    }
}