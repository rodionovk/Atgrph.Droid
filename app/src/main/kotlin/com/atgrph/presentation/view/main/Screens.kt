package com.atgrph.presentation.view.main

/**
 * ATGRPH. Created by KONSTANTIN on 24.11.2017.
 */
object Screens {
    val HOME = "home"
    val SEARCH = "search"
    val PEN = "pen"
    val NOTIFICATION = "notification"
    val PROFILE = "profile"

    val WELCOME_AUTOGRAPHS = "welcome_autographs"
    val WELCOME_POINTS = "welcome_points"
    val WELCOME_LIKES = "welcome_likes"
    val WELCOME_AUTHENTICATION = "welcome_authentication"
}