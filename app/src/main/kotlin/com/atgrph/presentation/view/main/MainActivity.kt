package com.atgrph.presentation.view.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.atgrph.AtgrphApplication
import com.atgrph.R
import com.atgrph.presentation.navigator.IBackButton
import com.atgrph.presentation.presenter.main.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Back
import ru.terrakok.cicerone.commands.Replace
import ru.terrakok.cicerone.commands.SystemMessage
import javax.inject.Inject

class MainActivity : AppCompatActivity(), IMainView, View.OnClickListener {

    @Inject lateinit var router: Router
    @Inject lateinit var navigationHolder: NavigatorHolder

    private lateinit var mainPresenter: MainPresenter
    private var listStateNavigator: MutableList<String> = ArrayList()


    companion object {
        fun start(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        (application as AtgrphApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)

        initViews()
        initContainers()

        mainPresenter = MainPresenter(router, this)
        mainPresenter.init()
    }

    private fun initViews() {
        setContentView(R.layout.activity_main)

        imgHome.setOnClickListener(this)
        imgSearch.setOnClickListener(this)
        imgPen.setOnClickListener(this)
        imgNotification.setOnClickListener(this)
        imgProfile.setOnClickListener(this)
    }

    private var feedFragment: TabContainerFragment? = null
    private var searchFragment: TabContainerFragment? = null
    private var penFragment: TabContainerFragment? = null
    private var notificationFragment: TabContainerFragment? = null
    private var profileFragment: TabContainerFragment? = null

    private fun initContainers() {

        val fragmentManager = supportFragmentManager

        feedFragment = fragmentManager.findFragmentByTag(Screens.HOME) as TabContainerFragment?
        if(feedFragment == null) {
            feedFragment = TabContainerFragment.newInstance(Screens.HOME)
            fragmentManager.beginTransaction()
                    .add(R.id.mainContainer, feedFragment, Screens.HOME)
                    .detach(feedFragment)
                    .commitNow()
        }

        searchFragment = fragmentManager.findFragmentByTag(Screens.SEARCH) as TabContainerFragment?
        if(searchFragment == null) {
            searchFragment = TabContainerFragment.newInstance(Screens.SEARCH)
            fragmentManager.beginTransaction()
                    .add(R.id.mainContainer, searchFragment, Screens.SEARCH)
                    .detach(searchFragment)
                    .commitNow()
        }

        penFragment = fragmentManager.findFragmentByTag(Screens.PEN) as TabContainerFragment?
        if(penFragment == null) {
            penFragment = TabContainerFragment.newInstance(Screens.PEN)
            fragmentManager.beginTransaction()
                    .add(R.id.mainContainer, penFragment, Screens.PEN)
                    .detach(penFragment)
                    .commitNow()
        }

        notificationFragment = fragmentManager.findFragmentByTag(Screens.NOTIFICATION) as TabContainerFragment?
        if(notificationFragment == null) {
            notificationFragment = TabContainerFragment.newInstance(Screens.NOTIFICATION)
            fragmentManager.beginTransaction()
                    .add(R.id.mainContainer, notificationFragment, Screens.NOTIFICATION)
                    .detach(notificationFragment)
                    .commitNow()
        }

        profileFragment = fragmentManager.findFragmentByTag(Screens.PROFILE) as TabContainerFragment?
        if(profileFragment == null) {
            profileFragment = TabContainerFragment.newInstance(Screens.PROFILE)
            fragmentManager.beginTransaction()
                    .add(R.id.mainContainer, profileFragment, Screens.PROFILE)
                    .detach(profileFragment)
                    .commitNow()
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigationHolder.removeNavigator()
        super.onPause()
    }

    override fun onClick(v: View?) {
        mainPresenter?.tabClick(v?.id)
    }

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)
        if(fragment != null && fragment is IBackButton && (fragment as IBackButton).onBackPressed())
            return
        else
            mainPresenter?.backPressed()
    }

    private fun replaceFragment(key: String) {

        val fragmentManager = supportFragmentManager

        when(key) {
            Screens.HOME -> {

                fragmentManager.beginTransaction()
                        .detach(searchFragment)
                        .detach(penFragment)
                        .detach(notificationFragment)
                        .detach(profileFragment)
                        .attach(feedFragment)
                        .commit()
            }
            Screens.SEARCH -> {

                fragmentManager.beginTransaction()
                        .detach(feedFragment)
                        .detach(penFragment)
                        .detach(notificationFragment)
                        .detach(profileFragment)
                        .attach(searchFragment)
                        .commit()
            }
            Screens.PEN -> {
                fragmentManager.beginTransaction()
                        .detach(feedFragment)
                        .detach(searchFragment)
                        .detach(notificationFragment)
                        .detach(profileFragment)
                        .attach(penFragment)
                        .commit()
            }
            Screens.NOTIFICATION -> {

                fragmentManager.beginTransaction()
                        .detach(searchFragment)
                        .detach(penFragment)
                        .detach(feedFragment)
                        .detach(profileFragment)
                        .attach(notificationFragment)
                        .commit()
            }
            Screens.PROFILE -> {

                fragmentManager.beginTransaction()
                        .detach(searchFragment)
                        .detach(penFragment)
                        .detach(notificationFragment)
                        .detach(feedFragment)
                        .attach(profileFragment)
                        .commit()
            }
        }
    }

    private val navigator: Navigator = Navigator { command ->
        if (command is Back) {

            if (listStateNavigator.size > 1){

                listStateNavigator.removeAt(listStateNavigator.lastIndex)
                replaceFragment(listStateNavigator.last())

            } else {
                finish()
            }

        } else if (command is SystemMessage) {

        } else if (command is Replace) {

            if(listStateNavigator.size > 0 && listStateNavigator.last() == command.screenKey)
                return@Navigator

            if(listStateNavigator.contains(command.screenKey)) {

                val currentPosition = listStateNavigator.indexOf(command.screenKey)
                listStateNavigator.add(command.screenKey)
                listStateNavigator.removeAt(currentPosition)

            } else {
                listStateNavigator.add(command.screenKey)
            }

            replaceFragment(command.screenKey)
        }
    }

}
