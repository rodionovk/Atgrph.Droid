package com.atgrph.presentation.view.base

import android.support.v7.app.AppCompatActivity

/**
 * ATGRPH. Created by KONSTANTIN on 28.11.2017.
 */
open class BaseActivity : AppCompatActivity() {
}