package com.atgrph.presentation.view.welcome

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.atgrph.AtgrphApplication
import com.atgrph.R
import com.atgrph.presentation.presenter.welcome.WelcomePresenter
import com.atgrph.presentation.view.main.Screens
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.commands.Replace
import javax.inject.Inject

class WelcomeActivity : AppCompatActivity(), INextScreen {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, WelcomeActivity::class.java)
            context.startActivity(intent)
        }
    }

    @Inject lateinit var router: Router
    @Inject lateinit var navigationHolder: NavigatorHolder

    private lateinit var welcomePresenter: WelcomePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as AtgrphApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_welcome)
        welcomePresenter = WelcomePresenter(router)
        welcomePresenter.init()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigationHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigationHolder.removeNavigator()
        super.onPause()
    }

    private fun replaceFragment(key: String) {
        val fragmentManager = supportFragmentManager

        val welcomeInfoFragment = when(key) {
            Screens.WELCOME_AUTHENTICATION -> WelcomeAuthFragment()
            else -> WelcomeInfoFragment.newInstance(key)
        }


        fragmentManager.beginTransaction()
                .replace(R.id.frmContainer, welcomeInfoFragment, key)
                .commit()
    }

    private val navigator: Navigator = Navigator { command ->
        if(command is Replace) {
            replaceFragment(command.screenKey)
        }
    }

    override fun onNextScreen(key: String) {
        welcomePresenter.nextScreen(key)
    }

}
