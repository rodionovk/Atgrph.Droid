package com.atgrph.presentation.view.base

import android.os.Bundle
import android.support.v4.app.Fragment
import com.atgrph.presentation.navigator.IBackButton
import com.atgrph.presentation.navigator.IRouterProvider
import com.atgrph.presentation.presenter.base.BasePresenter

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
open class BaseFragment : Fragment(), IBackButton {

    lateinit var basePresenter: BasePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val router = (parentFragment as IRouterProvider).getRouter()
        basePresenter = BasePresenter(router)
    }

    override fun onBackPressed(): Boolean {
        basePresenter.exit()
        return true
    }
}