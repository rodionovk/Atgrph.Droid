package com.atgrph.presentation.view.feed

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atgrph.R
import com.atgrph.presentation.navigator.IBackButton
import com.atgrph.presentation.navigator.IRouterProvider
import com.atgrph.presentation.presenter.feed.FeedPresenter
import com.atgrph.presentation.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_feed.*

/**
 * ATGRPH. Created by KONSTANTIN on 24.11.2017.
 */
class FeedFragment : BaseFragment(), IFeedView {

    private var text: String? = null
    private lateinit var feedPresenter: FeedPresenter

    companion object {

        private val EXTRA_TEXT = "text"

        fun newInstance(info: String): FeedFragment {
            val fragment = FeedFragment()
            val args = Bundle()
            args.putString(EXTRA_TEXT, info)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        text = arguments.getString(EXTRA_TEXT,"");
        val router = (parentFragment as IRouterProvider).getRouter()
        feedPresenter = FeedPresenter(router, this)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_feed, container,false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtCenter?.setText(text)
        btnProfile.setOnClickListener { feedPresenter.gotoProfile() }
    }

}