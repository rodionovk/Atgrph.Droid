package com.atgrph.presentation.view.launch

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.atgrph.presentation.view.main.MainActivity
import com.atgrph.presentation.view.welcome.WelcomeActivity

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        WelcomeActivity.start(this)
        finish()
    }
}
