package com.atgrph.presentation.view.welcome

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atgrph.R
import com.atgrph.presentation.view.main.Screens
import kotlinx.android.synthetic.main.fragment_welcome_info.*

/**
 * ATGRPH. Created by KONSTANTIN on 28.11.2017.
 */
class WelcomeInfoFragment : Fragment() {

    companion object {

        private val EXTRA_TYPE = "type"

        fun newInstance(type: String): WelcomeInfoFragment {
            val fragment = WelcomeInfoFragment()
            val args = Bundle()
            args.putString(EXTRA_TYPE, type)
            fragment.arguments = args
            return fragment
        }
    }

    private var type: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        type = arguments.getString(EXTRA_TYPE)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_welcome_info, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imgMain.setImageResource(
                when(type){
                    Screens.WELCOME_AUTOGRAPHS -> R.drawable.welcome_autographs
                    Screens.WELCOME_POINTS -> R.drawable.welcome_points
                    Screens.WELCOME_LIKES -> R.drawable.welcome_likes
                    else -> 0
                }
        )

        txtTitle.setText(
                when(type){
                    Screens.WELCOME_AUTOGRAPHS -> R.string.welcome_title_autograph
                    Screens.WELCOME_POINTS -> R.string.welcome_title_points
                    Screens.WELCOME_LIKES -> R.string.welcome_title_like
                    else -> 0
                }

        )

        txtDescription.setText(
                when(type){
                    Screens.WELCOME_AUTOGRAPHS -> R.string.welcome_description_autograph
                    Screens.WELCOME_POINTS -> R.string.welcome_description_points
                    Screens.WELCOME_LIKES -> R.string.welcome_description_like
                    else -> 0
                }

        )

        btnNext.setText(
                when(type){
                    Screens.WELCOME_AUTOGRAPHS -> R.string.welcome_button_autograph
                    Screens.WELCOME_POINTS -> R.string.welcome_button_points
                    Screens.WELCOME_LIKES -> R.string.welcome_button_like
                    else -> 0
                }
        )

        btnNext.setOnClickListener {
            (activity as INextScreen).onNextScreen(
                    when(type){
                        Screens.WELCOME_AUTOGRAPHS -> Screens.WELCOME_POINTS
                        Screens.WELCOME_POINTS -> Screens.WELCOME_LIKES
                        Screens.WELCOME_LIKES -> Screens.WELCOME_AUTHENTICATION
                        else -> ""
                    }
            )
        }
    }
}