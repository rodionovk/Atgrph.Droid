package com.atgrph.presentation.view.welcome

/**
 * ATGRPH. Created by KONSTANTIN on 28.11.2017.
 */
interface INextScreen {
    fun onNextScreen(key: String)
}