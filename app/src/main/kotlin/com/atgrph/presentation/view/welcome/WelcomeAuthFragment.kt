package com.atgrph.presentation.view.welcome

import android.annotation.SuppressLint
import android.os.Bundle
import android.content.Intent
import android.provider.Settings
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atgrph.R
import com.atgrph.data.AtgrphRepository
import com.atgrph.data.models.AuthToken
import com.atgrph.data.models.User
import com.vk.sdk.*
import com.vk.sdk.api.VKError
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_authorization.*
import retrofit2.HttpException

/**
 * ATGRPH. Created by KONSTANTIN on 30.11.2017.
 */
class WelcomeAuthFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_authorization, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imgVk.setOnClickListener {
            //VKSdk.login(this, VKScope.NOTIFY, VKScope.PHOTOS, VKScope.WALL, VKScope.EMAIL)
            val intent = Intent(activity.application, VKServiceActivity::class.java)
            intent.putExtra("arg1", VKServiceActivity.VKServiceType.Authorization.name)
            intent.putExtra("arg4", VKSdk.isCustomInitialize())
            intent.putStringArrayListExtra("arg2", arrayListOf(VKScope.NOTIFY, VKScope.PHOTOS, VKScope.WALL, VKScope.EMAIL, VKScope.OFFLINE))
            startActivityForResult(intent, VKServiceActivity.VKServiceType.Authorization.getOuterCode());
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(!VKSdk.onActivityResult(requestCode, resultCode, data,  object : VKCallback<VKAccessToken> {

            override fun onError(error: VKError?) {

            }

            @SuppressLint("HardwareIds")
            override fun onResult(res: VKAccessToken?) {

                AtgrphRepository().authWithSocialToken(res!!.accessToken, Settings.Secure.getString(activity.contentResolver, Settings.Secure.ANDROID_ID), User.UserAuthType.VK.value)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { response ->
                                    var date = response?.createDate
                                    var dd = response?.token

                                    dd = dd + ""

                                    AtgrphRepository().getUser(response?.userId, response.token)
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe( { r ->
                                                val r = r.length
                                            }, { th ->
                                                var date = (th as HttpException).response().errorBody()?.string()
                                            } )
                                },
                                { throwable ->
                                    var date = (throwable as HttpException).response().errorBody()?.string()

                                    date = date + ""
                                }
                        )



            }

        })) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}