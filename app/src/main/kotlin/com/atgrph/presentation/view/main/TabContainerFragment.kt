package com.atgrph.presentation.view.main

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.atgrph.AtgrphApplication
import com.atgrph.R
import com.atgrph.presentation.navigator.IBackButton
import com.atgrph.presentation.navigator.IRouterProvider
import com.atgrph.presentation.navigator.TabCiceroneHolder
import com.atgrph.presentation.view.feed.FeedFragment
import com.atgrph.presentation.view.notification.NotificationFragment
import com.atgrph.presentation.view.pen.PenFragment
import com.atgrph.presentation.view.profile.ProfileFragment
import com.atgrph.presentation.view.search.SearchFragment
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator
import javax.inject.Inject

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
class TabContainerFragment : Fragment(), IRouterProvider, IBackButton {

    @Inject lateinit var tabHolder: TabCiceroneHolder
    private var navigator: Navigator? = null

    companion object {

        private val EXTRA_NAME = "name"

        fun newInstance(name: String): TabContainerFragment {

            val fragment = TabContainerFragment()
            val args = Bundle()
            args.putString(EXTRA_NAME, name)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (activity.application as AtgrphApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    private fun getContainerName() : String {
        return  arguments.getString(EXTRA_NAME)
    }

    private fun getCicerone(): Cicerone<Router>? {
        return tabHolder.getCicerone(getContainerName())
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_tab_container, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if(childFragmentManager.findFragmentById(R.id.frmContainer) == null) {
            getCicerone()?.router?.replaceScreen(getContainerName())
        }
    }

    override fun onResume() {
        super.onResume()
        getCicerone()?.navigatorHolder?.setNavigator(getNavigator())
    }

    override fun onPause() {
        getCicerone()?.navigatorHolder?.removeNavigator()
        super.onPause()
    }

    private fun getNavigator() : Navigator {
        if(navigator == null) {
            navigator = object : SupportAppNavigator(activity, childFragmentManager, R.id.frmContainer) {

                override fun createFragment(screenKey: String?, data: Any?): Fragment? {
                    when(screenKey) {
                        Screens.HOME -> { return FeedFragment.newInstance(screenKey) }
                        Screens.SEARCH -> { return SearchFragment.newInstance(screenKey) }
                        Screens.PEN -> { return PenFragment.newInstance(screenKey) }
                        Screens.NOTIFICATION -> { return NotificationFragment.newInstance(screenKey) }
                        Screens.PROFILE -> { return ProfileFragment.newInstance(screenKey) }
                    }
                    return null
                }

                override fun createActivityIntent(screenKey: String?, data: Any?): Intent? {
                    return null
                }

                override fun exit() {
                    (activity as MainActivity).router.exit()
                }

            }
        }

        return navigator as Navigator
    }

    override fun getRouter(): Router {
        return getCicerone()!!.router
    }

    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.frmContainer)
        if(fragment != null && fragment is IBackButton && (fragment as IBackButton).onBackPressed())
            return true
        else {
            (activity as MainActivity).router.exit()
            return true
        }
    }

}