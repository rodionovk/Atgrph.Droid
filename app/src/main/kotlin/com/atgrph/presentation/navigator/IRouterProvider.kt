package com.atgrph.presentation.navigator

import ru.terrakok.cicerone.Router

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
interface IRouterProvider {
    fun getRouter(): Router
}