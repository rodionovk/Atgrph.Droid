package com.atgrph.presentation.navigator

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
class TabCiceroneHolder {

    val containers: HashMap<String, Cicerone<Router>> by lazy {
        HashMap<String, Cicerone<Router>>()
    }

    fun getCicerone(containerTag: String) : Cicerone<Router>? {
        if(!containers.containsKey(containerTag)) {
            containers.put(containerTag, Cicerone.create())
        }
        return containers[containerTag]
    }
}