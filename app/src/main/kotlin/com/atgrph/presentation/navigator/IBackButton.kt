package com.atgrph.presentation.navigator

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
interface IBackButton {
    fun onBackPressed(): Boolean
}