package com.atgrph.di

import com.atgrph.presentation.navigator.TabCiceroneHolder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * ATGRPH. Created by KONSTANTIN on 27.11.2017.
 */
@Module
class TabNavigationModule {

    @Provides
    @Singleton
    fun provideTabNavigationHolder() : TabCiceroneHolder {
        return TabCiceroneHolder()
    }
}
