package com.atgrph.di

import android.content.Context
import com.atgrph.AtgrphApplication
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

/**
 * ATGRPH. Created by KONSTANTIN on 23.11.2017.
 */
@Module
class AppModule(private val application: AtgrphApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext() : Context {
        return application
    }
}