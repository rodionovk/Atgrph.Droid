package com.atgrph.di

import com.atgrph.AtgrphApplication
import com.atgrph.presentation.view.main.MainActivity
import com.atgrph.presentation.view.main.TabContainerFragment
import com.atgrph.presentation.view.welcome.WelcomeActivity
import dagger.Component
import javax.inject.Singleton

/**
 * ATGRPH. Created by KONSTANTIN on 23.11.2017.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NavigationModule::class, TabNavigationModule::class))
interface AppComponent {
    fun inject(app: AtgrphApplication)
    fun inject(mainActivity: MainActivity)
    fun inject(welcomeActivity: WelcomeActivity)
    fun inject(fragment: TabContainerFragment)
}