package com.atgrph.di

import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

/**
 * ATGRPH. Created by KONSTANTIN on 23.11.2017.
 */
@Module
class NavigationModule {

    val cicerone: Cicerone<Router> by lazy {
        Cicerone.create()
    }

    @Provides
    @Singleton
    fun provideRouter() : Router {
        return cicerone.router
    }

    @Provides
    @Singleton
    fun provideNavigationHolder() : NavigatorHolder {
        return cicerone.navigatorHolder
    }
}