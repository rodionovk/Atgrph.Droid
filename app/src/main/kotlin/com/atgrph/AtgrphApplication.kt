package com.atgrph

import android.app.Application
import com.atgrph.di.AppComponent
import com.atgrph.di.DaggerAppComponent
import com.vk.sdk.VKSdk

/**
 * ATGRPH. Created by KONSTANTIN on 23.11.2017.
 */
 
class AtgrphApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        val appComponent = DaggerAppComponent.builder().build()
        appComponent.inject(this)
        this.appComponent = appComponent

        VKSdk.initialize(this)
    }

}